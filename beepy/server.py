from json import JSONDecoder

from flask import Flask
from flask import request

from beepy.game import Game

app = Flask(__name__)
decoder = JSONDecoder()
game = Game()

@app.route("/")
def hello_world():
    return "<p>Hello!</p>"

@app.route("/api/game", methods=["GET", "POST"])
def get_game():
    if request.method == "GET":
        return game.get_game()
    else:
        s = request.get_data(as_text=True, parse_form_data=True)
        data = decoder.decode(s)
        answer = data["answer"]
        return game.check_answer(answer)

@app.route("/api/game/words")
def get_words():
    return game.get_some_words()

@app.route("/api/game/pangram")
def get_pangram():
    return game.pangram

