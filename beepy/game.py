from pathlib import Path
from random import Random

words_file = Path("./data/words_alpha.txt")
randomizer = Random()

class Game:
    def __init__(self):
        word_list = self.__load_words__()
        pangram = randomizer.choice(get_pangrams(word_list))
        print(pangram)
        self.letters = list(pangram)
        randomizer.shuffle(self.letters)
        self.pangram = set(self.letters)
        self.focus_letter = self.letters[0]
        self.words = create_valid_words_list(self.pangram, word_list)

    def get_game(self):
        return {
            "game": {
                "letters": self.letters,
                "focus_letter": self.focus_letter,
            }
        }

    def check_answer(self, answer):
        letter_set = set(answer)

        if letter_set.issubset(self.pangram) \
                & len(answer) >= 4 \
                & (self.focus_letter in answer) \
                & (answer in self.words):
            return {"valid": True, "points": calculate_points(answer)}
        
        return {"valid": False, "points": 0}

    def get_some_words(self):
        return self.words[0:10]

    def __load_words__(self):
        f = open(words_file)
        words = []
        for line in f:
            line = line.strip()
            if len(line) >= 4:
                words.append(line)
        return words


def calculate_points(answer):
    if len(answer) == 4:
        return 1
    elif len(set(answer)) == 7:
        return 7 + len(answer)
    else:
        return len(answer)


def create_valid_words_list(pangram, words):
    return [
        word for word in words
        if set(word).issubset(pangram)
    ]


def get_pangrams(words):
    return [word for word in words if len(word) == 7 & len(set(word)) == 7]

